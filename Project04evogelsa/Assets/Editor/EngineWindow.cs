﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


public class EngineWindow : EditorWindow
{
    public List<ScriptEffects> effectList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    Engine engineScript;

    //Track items we are editing
    int movementItem = 0, facingItem = 0, effectItem = 0;

    public Vector2 minimumSize = new Vector2(750F, 350F);

    //setup button skin
    GUIStyle miniLeft, miniCenter, miniRight;

    //position variables
    float offsetX = 0f, offsetY = 0f, ELEMENT_HEIGHT = 17f;

    public static void Init()
    {
        EngineWindow window = (EngineWindow)EditorWindow.GetWindow(typeof(EngineWindow));
        window.Show();
    }

    void OnFocus()
    {
        //get a reference to the script we are supposed to be editing
        engineScript = GameObject.Find("EngineObject").GetComponent<Engine>();

        //setup buttons for prettyness and things
        miniLeft = new GUIStyle(EditorStyles.miniButtonLeft);
        miniCenter = new GUIStyle(EditorStyles.miniButtonMid);
        miniRight = new GUIStyle(EditorStyles.miniButtonRight);

        //pull information from the scropt we are editing into local variables
        movementList = engineScript.movementList;
        effectList = engineScript.effectList;
        facingList = engineScript.facingList;

        //pull current waypoint
        movementItem = engineScript.lastMove;
        facingItem = engineScript.lastFacing;
        effectItem = engineScript.lastEffect;

        //make sure nothing is null
        if (movementList == null)
            movementList = new List<ScriptMovement>();
        if (effectList == null)
            effectList = new List<ScriptEffects>();
        if (facingList == null)
            facingList = new List<ScriptFacings>();

        //make sure that there is at least one element in the list
        if (movementList.Count < 1)
            movementList.Add(new ScriptMovement());
        if (effectList.Count < 1)
            effectList.Add(new ScriptEffects());
        if (facingList.Count < 1)
            facingList.Add(new ScriptFacings());
    }

    void OnGUI()
    {
        //built-in variable for editor window
        minSize = minimumSize;

        offsetX = 0;
        offsetY = 0f;
        MovementGUI();
        EffectsGUI();
        FacingsGUI();
    }

    void MovementGUI()
    {
        //setup ects for showing information (x, y, width, height)
        Rect movementLabelPos = new Rect(50, 10, 250f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT;

        //setup a draw line
        Vector2 linePointTop = new Vector2(250, 3);
        Vector2 linePointBottom = new Vector2(250, 350);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.black, 1f, true);

        EditorGUI.LabelField(movementLabelPos, "MOVEMENT WAYPOINTS");

        //notating what thingy we're on
        Rect numberPos = new Rect(115f, offsetY + 10f, 60f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        EditorGUI.LabelField(numberPos, "" + (movementItem + 1) + "/" + movementList.Count);


        //waypoint name
        Rect nameLabelPos = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
        
        EditorGUI.LabelField(nameLabelPos, "Waypoint Name: ");

        //Input field
        Rect nameInputPos = new Rect(offsetX + 110f, offsetY, 135f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT;

        movementList[movementItem].name = EditorGUI.TextField(nameInputPos, movementList[movementItem].name);

        //waypoint type label


        //waypoint type
        Rect waypointPos = new Rect(offsetX + 15f, offsetY + 8f, 220f, ELEMENT_HEIGHT);
        movementList[movementItem].type = (MovementTypes)EditorGUI.EnumPopup(waypointPos, movementList[movementItem].type);
        offsetY += ELEMENT_HEIGHT * 2f;

        //specific Display Info
        switch(movementList[movementItem].type)
        {
            case MovementTypes.STRAIGHT:
                Rect startLabel = new Rect(offsetX, offsetY, 100F, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(startLabel, "End Point: ");

                Rect startPos = new Rect(offsetX, offsetY, 150, ELEMENT_HEIGHT);
                offsetX -= 100;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(startPos, movementList[movementItem].endPoint, typeof(GameObject),true);
                break;
        }

        offsetX += 30;
        Rect prevPos = new Rect(offsetX, 180f, 50f, ELEMENT_HEIGHT + 8f);
        Rect addPos = new Rect(offsetX + 50f, 180f, 90f, ELEMENT_HEIGHT + 8f);
        Rect nextPos = new Rect(offsetX + 140f, 180f, 50f, ELEMENT_HEIGHT + 8f);
        //display 3 little buttons
        if (movementItem != 0)
        {
            if (GUI.Button(prevPos, "Prev", miniLeft))
            {
                //if (movementItem == 0)
                //    movementItem--;
                movementItem--;
            } 
        }
        if (GUI.Button(addPos, "Add", miniCenter))
        {
            movementList.Insert(movementItem + 1, new ScriptMovement());
        }
        if (movementItem != movementList.Count - 1)
        {
            if (GUI.Button(nextPos, "Next", miniRight))
            {
                movementItem++;
            } 
        }//end waypoint switching

        Rect deleteRect = new Rect(offsetX + 35f, 180f + ELEMENT_HEIGHT + 18f, 120f, ELEMENT_HEIGHT + 8f);
        GUI.color = Color.red + Color.white;
        if (movementList.Count > 1)
        {
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (movementItem == movementList.Count - 1)
                {
                    movementList.RemoveAt(movementItem);
                    movementItem--;
                }
                else if (movementList.Count > 1)
                {
                    movementList.RemoveAt(movementItem);
                }
                else
                {
                    Debug.Log("Cannot delete last waypoint");
                }
            } 
        }

        GUI.color = Color.white;
    }
    void FacingsGUI()
    {
        Vector2 linePointTop = new Vector2(500, 3);
        Vector2 linePointBottom = new Vector2(500, 350);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.black, 1f, true);
        offsetX = 255;
        offsetY = 0;

        Rect facingTitlePos = new Rect(offsetX + 50f, offsetY + 10f, 240f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(facingTitlePos, "FACING WAYPOINTS");
        offsetY += ELEMENT_HEIGHT;

        //notating what thingy we're on
        Rect numberPos = new Rect(115f + offsetX, offsetY + 10f, 60f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        EditorGUI.LabelField(numberPos, "" + (facingItem + 1) + "/" + facingList.Count);

        //waypoint name
        Rect nameLabelPos = new Rect(offsetX, offsetY, 105f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(nameLabelPos, "Waypoint Name: ");

        //facing name
        Rect facingNamePos = new Rect(offsetX + 110f, offsetY, 130f, ELEMENT_HEIGHT);
        facingList[facingItem].name = EditorGUI.TextField(facingNamePos, facingList[facingItem].name);
        offsetY += ELEMENT_HEIGHT;

        //facing type
        Rect facingTypeLabelPos = new Rect(offsetX, offsetY + 5f, 95f, ELEMENT_HEIGHT);
        offsetX += 80f;
        EditorGUI.LabelField(facingTypeLabelPos, "FacingType: ");

        Rect facingTypePos = new Rect(offsetX, offsetY + 5f, 155f, ELEMENT_HEIGHT);
        offsetX -= 80f;
        offsetY += ELEMENT_HEIGHT * 2f;
        facingList[facingItem].type = (FacingTypes)EditorGUI.EnumPopup(facingTypePos, facingList[facingItem].type);

        switch (facingList[facingItem].type)
        {
            case FacingTypes.LOOKAT:

                //display a target to look at
                Rect windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX += 110f;
                EditorGUI.LabelField(windowDisplay, "Target to Look at: ");

                windowDisplay = new Rect(offsetX, offsetY, 130f, ELEMENT_HEIGHT);
                offsetX -= 110f;//maybe gotta change this
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                //facing time
                windowDisplay = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                offsetX += 130f;
                EditorGUI.LabelField(windowDisplay, "Time to look over: ");

                windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX -= 130f;
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].time = EditorGUI.FloatField(windowDisplay, facingList[facingItem].time);
                break;
        }

        //3 buttons
        offsetX += 30;
        Rect prevPos = new Rect(offsetX, 180f, 50f, ELEMENT_HEIGHT + 8f);
        Rect addPos = new Rect(offsetX + 50f, 180f, 90f, ELEMENT_HEIGHT + 8f);
        Rect nextPos = new Rect(offsetX + 140f, 180f, 50f, ELEMENT_HEIGHT + 8f);
        //display 3 little buttons
        //previous
        if (facingItem != 0)
        {
            if (GUI.Button(prevPos, "Prev", miniLeft))
            {
                facingItem--;
            }
        }
        //add
        if (GUI.Button(addPos, "Add", miniCenter))
        {
            facingList.Insert(facingItem + 1, new ScriptFacings());
        }
        //next
        if (facingItem != facingList.Count - 1)
        {
            if (GUI.Button(nextPos, "Next", miniRight))
            {
                facingItem++;
            }
        }//end waypoint switching

        Rect deleteRect = new Rect(offsetX + 35f, 180f + ELEMENT_HEIGHT + 18f, 120f, ELEMENT_HEIGHT + 8f);
        GUI.color = Color.red + Color.white;
        if (facingList.Count > 1)
        {
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (facingItem == facingList.Count - 1)
                {
                    facingList.RemoveAt(facingItem);
                    facingItem--;
                }
                else if (facingList.Count > 1)
                {
                    facingList.RemoveAt(facingItem);
                }
                else
                {
                    Debug.Log("Cannot delete last waypoint");
                }
            }
        }

        GUI.color = Color.white;

    }
    void EffectsGUI()
    {
        offsetX += 500;
    }

    void OnLostFocus()
    {
        PushData();
    }

    void PushData()
    {
        //push the stuff back
        engineScript.movementList = movementList;
        engineScript.facingList = facingList;
        engineScript.effectList = effectList;

        //push last waypoints to engine
        movementItem = engineScript.lastMove;
        facingItem = engineScript.lastFacing;
        effectItem = engineScript.lastEffect;
    }
}
