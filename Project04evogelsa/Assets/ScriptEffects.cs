﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptEffects
{
    public EffectTypes type;

    public string name;

    public float duration;
    public float intensity;

}
