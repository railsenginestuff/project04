﻿public enum MovementTypes
{    
    STRAIGHT,
    BEZIER,
    WAIT 
}

public enum FacingTypes
{
    LOOKAT,
    FREELOOK,
    WAIT 
}

public enum EffectTypes
{
    SHAKE,
    SPLATTER,
    FADE,
    WAIT
}
