﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptFacings
{
    public FacingTypes type;

    public string name;

    public GameObject lookTarget;
    public float time;
}
