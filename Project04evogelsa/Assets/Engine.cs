﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Engine : MonoBehaviour
{
    public List<ScriptEffects> effectList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    int currentWaypoint;

    GameObject player;
    public int lastMove = 0, lastFacing = 0, lastEffect = 0;

    void Start()
    {
        player = GameObject.FindWithTag("Player");

        StartCoroutine("MovementEngine");
        StartCoroutine("FacigEngine");
        StartCoroutine("EffectEngine");
    }

    IEnumerator MovementEngine()
    {
        for(int i = 0; i < movementList.Count; i++)
        {
            switch(movementList[i].type)
            {
                case MovementTypes.STRAIGHT:
                    break;

            }
        }

        yield return null;
    }

    IEnumerator FacingEngine()
    {
        for (int i = 0; i < facingList.Count; i++)
        {
            switch (facingList[i].type)
            {
                case FacingTypes.FREELOOK:
                    break;
                case FacingTypes.LOOKAT:
                    yield return StartCoroutine(LookAtTarget(facingList[i].time, facingList[i].lookTarget.transform.position));
                    break;

            }
        }
    }

    IEnumerator EffectEngine()
    {
        for (int i = 0; i < effectList.Count; i++)
        {
            switch (effectList[i].type)
            {
                case EffectTypes.FADE:
                    break;

            }
        }

        yield return null;
    }

    IEnumerator StraightLine(float time, Vector3 endPos)
    {
        float step = 0.0f;
        Vector3 startPos = player.transform.position;
        while(step < time)
        {
            player.transform.position = Vector3.Lerp(startPos, endPos, (step / time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        player.transform.position = endPos;
    }

    IEnumerator LookAtTarget(float time, Vector3 lookPosition)
    {
        float step = 0.0f;
        Quaternion startRot = player.transform.rotation;
        Quaternion targetRot = Quaternion.LookRotation((lookPosition - player.transform.position).normalized);
        while(step < time)
        {
            player.transform.rotation = Quaternion.Lerp(startRot, targetRot, (step / time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //yield wait for the seconds looking at them
        step = 0.0f;
        while (step < time)
        {
            player.transform.rotation = Quaternion.Lerp(targetRot, Quaternion.Euler(player.transform.forward), (step / time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        player.transform.rotation = Quaternion.Euler(player.transform.forward);
    }
}

